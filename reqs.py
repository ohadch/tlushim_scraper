import os


if not os.path.exists('config'):
    os.mkdir('config')

reqs_installed_log_path = 'config/reqs_installed'

if not os.path.exists(reqs_installed_log_path):
    requirements = open('utils/requirements.txt', 'r').read().split("\n")
    [os.system('pip install {}'.format(requirement)) for  requirement in requirements]
    f = open(reqs_installed_log_path, 'w')
    f.close()
