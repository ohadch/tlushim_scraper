#!/usr/bin/python
# -*- coding: utf-8 -*-


import bs4
import csv
import json
import pandas
import sqlite3
import os
from selenium import webdriver
from pyautogui import locateOnScreen


# ----------- Config -----------

if not os.path.exists('config'):
    os.mkdir('config')

# ---------- Classes -----------


class GeckoDriverInstaller:

    def __init__(self):

        self.install_dir = ''


class TableScraper:

    def __init__(self):
        if os.path.exists("tlushim.db"):
            os.remove("tlushim.db")
        self.driver = ''
        self.headers = []
        self.rows = [[]]
        self.table = bs4.BeautifulSoup("", 'html.parser')
        self.conn = sqlite3.connect('tlushim.db')
        self.csv_path = 'out.csv'
        self.credentials_file_path = 'config/credentials.json'
        self.table_name = 'existence'
        self.result = ''
        self.intel = {
            'url': r'https://www.tlushim.co.il/index.php',
            'id_num_input_name': 'id_num',
            'password_input_name': 'password',
            'connect_button_name': 'connect',
            'main_table_class': "atnd",
        }
        self.credentials = {
            'id_num': '',
            'password': '',
        }

    def users_will_to_remember(self):
        remember_input = raw_input("Remember credentials? [y/n]")
        if remember_input.lower() == 'y':
            return True
        elif remember_input.lower() == 'n':
            return False
        else:
            print "Input must be either 'y' or 'n'."
            return self.users_will_to_remember()

    def get_credentials(self):
        if not os.path.exists(self.credentials_file_path):
            self.credentials['id_num'] = raw_input("ID Number: ")
            self.credentials['password'] = raw_input("Password: ")
            print
            if self.users_will_to_remember():
                creds_file = open(self.credentials_file_path, 'w')
                creds_file.write(json.dumps(self.credentials))
        else:
            self.credentials = json.loads(open(self.credentials_file_path, 'r').read())

    def login(self):
        self.driver.get(self.intel['url'])
        self.driver.find_element_by_name(self.intel['id_num_input_name']).send_keys(self.credentials['id_num'])
        self.driver.find_element_by_name(self.intel['password_input_name']).send_keys(self.credentials['password'])
        self.driver.find_element_by_name(self.intel['connect_button_name']).click()
        while True:
            if len(self.driver.find_elements_by_class_name(self.intel['main_table_class'])) > 0:
                break
        print "Signed in."

    def soup_table(self):
        raw = self.driver.page_source
        soup = bs4.BeautifulSoup(raw, 'html.parser')
        self.table = soup.find('table', class_=self.intel['main_table_class'])

    def parse_table(self):
        self.headers = [th.text.encode('utf-8').strip() for th in self.table.find_all('th')]
        self.rows = [[td.text.encode('utf-8').strip() for td in tr.find_all('td')] for tr in self.table.find_all('tr') if tr.find('th') is None]

    def write_to_csv(self):
        csv_file = open(self.csv_path, 'wb')
        writer = csv.writer(csv_file)
        writer.writerow(self.headers)
        [writer.writerow(row) for row in self.rows if "תיאור השינוי:" not in row]
        print "CSV Writing done."

    def csv_to_sql(self):
        csv_file = pandas.read_csv(self.csv_path, encoding='utf-8')
        csv_file.to_sql(name=self.table_name, con=self.conn, if_exists='append')

    def query_table(self):
        headers = ['expected', 'committed', 'lacking']
        c = self.conn.cursor()
        q = '''with a as (SELECT * FROM existence WHERE [סך הכל] is not null),
        
                b as (
        	        select [index] id,
        	        9.25 expected,
        	[סך הכל] committed,      
                	[אי השלמת תקן] lacking
                	from a
               )
               
                select sum(expected) {}, sum(committed) {}, sum(expected) = sum(committed) {} from b where id < (select max(id) from b)
        '''.format(headers[0], headers[1], headers[2])
        res = c.execute(q)
        data = list(list(res)[0])
        self.result = dict(zip(headers, data))
        return self.result

    def result_view(self):
        os.system('cls')
        balance = self.result['committed'] - self.result['expected']
        message = "On this month, "
        if balance > 0:
            message += "you committed extra {} hours than you should have up until now."
        elif balance == 0:
            message += "you committed the exact amount of hours that you should have up until now."
        else:
            message += "you need to commit at least more {} hours to be on balance."
        for val in ['expected', 'committed']:
            print "Hours {}: {}".format(val, self.result[val])
        print
        print message.format(balance)

    def run(self):
        self.get_credentials()
        self.driver = webdriver.Firefox()
        self.login()
        self.soup_table()
        self.driver.close()
        self.parse_table()
        self.write_to_csv()
        self.csv_to_sql()
        self.query_table()
        os.remove(self.csv_path)
        self.result_view()


if __name__ == '__main__':
    t = TableScraper()
    t.run()
